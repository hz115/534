\documentclass[a4paper,11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb,amsthm}
\usepackage[colorlinks,allcolors=blue,pagebackref]{hyperref}
\usepackage[top=1in,bottom=1in,left=1in,right=1in]{geometry}
\usepackage{graphicx}
\usepackage[linesnumbered,ruled,vlined,boxed]{algorithm2e}
\usepackage{mathtools}
\usepackage[]{algorithm2e}
\usepackage{lmodern}

\SetKwProg{Fn}{}{}{}\SetKwFunction{Quad}{\textsc{Quadtree}}%
\SetKwProg{Fn}{}{}{}\SetKwFunction{seqANN}{\textsc{Sequential-ANN}}%

\SetKwProg{Fn}{}{}{}\SetKwFunction{wspdone}{\textsc{WSPD-1}}%
\SetKwProg{Fn}{}{}{}\SetKwFunction{wspdtwo}{\textsc{WSPD-2}}%


\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\makeatletter
\let\oldabs\abs
\def\abs{\@ifstar{\oldabs}{\oldabs*}}

\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{theorem}{Theorem}
\newenvironment{customconj}[1]
  {\renewcommand\theconjecture{#1}\conjecture}
  {\endconjecture}


\newcommand{\myparagraph}[1]{\smallskip\noindent{\bf #1}}

\title{The Topological Approach Towards Graph Evasiveness}
\author{Fred Zhang}

\begin{document}
\date{} % No date
\maketitle
\begin{abstract}
    We survey several previous results on the Aanderaa-Karp-Rosenberg conjecture ({\it i.e.}, graph evasiveness conjecture). In particular, we focus on the algebraic-topological approach introduced by \cite{Kahn1984} and on its application to the special case on bipartite graph property. Some recent progress and the evasiveness of several specific graph properties are also discussed. 
\end{abstract}
\section{Introduction}
Let $V$ be a vertex set with $|V|=n$ and $\mathbf{G}(V)$ be the set of undirected graphs on $V$. A graph property $\mathcal{P}: \mathbf{G}(V) \rightarrow \{0,1\}$ is a mapping such that isomorphic graphs are mapped to the same value. Given such a property, we want to determine if $\mathcal{P}(G) =1$ for an unknown graph $G$. An algorithm can only gain information of the graph by asking questions of the form ``Is $e\in G$?'', \textit{i.e.}, querying entries of the adjacency matrix of $G$. Then an interesting quantity is the number $D(\mathcal{P})$ of questions an algorithm has to ask in the worst case to compute $\mathcal{P}(G)$.

Trivially, one may simply query all $\binom{n}{2}$ possible edges. We say that property $\mathcal{P}$ is \textit{evasive} if $D(\mathcal{P}) = \binom{n}{2}$. In particular, we are interested in \textit{monotone properties}. A graph property is said to be monotone if $\mathcal{P}(G) \geq \mathcal{P}(G')$ for any $G' \subseteq G$; in other words, the addition of edges does not destroy the property.  We also say that a graph property is  \textit{nontrivial} if it is not a constant.
Then the \textit{graph evasiveness conjecture} (also known as the \textit{Aanderaa-Karp–Rosenberg conjecture}) states that 
\begin{conjecture}[Evasiveness conjecture]
    \label{conj:1}
    Every nontrivial monotone graph property on $n$ vertices is evasive.
\end{conjecture}
The evasiveness conjecture was first proposed by Rosenberg for all nontrivial graph properties \cite{Rosenberg:1973:TRR:1008299.1008302}. It was later shown, however, by Aanderaa that there exists nontrivial property which is not evasive~\cite{best1974sharpened}. The first quadratic bound was shown by \cite{rivest1975generalization} proving that at least $n^2/16$ queries are required for testing any nontrivial monotone graph property. In 1983, Kahn, Saks and Sturtevant introduced the topological method and resolved the conjecture for the case where $n$ is a prime power \cite{Kahn1984}. We will survey several results of this line of works, exhibiting deep connections between topology and graph evasiveness.

The remainder of this paper will be organized as follows. In Section~\ref{sec:dtc}, we introduce \textit{decision tree complexity}, which gives a formal version of the problem.  The connections between graph properties and topology will be shown in Section~\ref{sec:topo}. In Section~\ref{sec:fixed}, we sketch several theorems from algebraic topology, in particular the \textit{fixed point theorem} for simplicial complex, that are necessary to understand our main results concerning graph evasiveness.  In Section~\ref{sec:evas}, we present the main evasiveness results with proofs. Finally, we discuss some more recent progress made by topological method in Section~\ref{sec:more}.

\section{Decision Tree Complexity}
\label{sec:dtc}
Informally, a \textit{Boolean decision tree} is a tree representing the logical structure of an algorithm computing a Boolean  function of the form $f: \{0,1\}^N \rightarrow \{0,1\}$. It is a rooted binary tree, where each internal node is associated with a variable of the input and has two outgoing edges labeled $0$ and $1$. Without loss of generality, we assume that the left edge is labeled with $1$. In addition, every leaf is labeled with either $0$ or $1$ that indicates the function value the decision tree computes.

\begin{figure}[htbp]
    \centering
    \includegraphics[scale=0.8]{./img/dst.eps}
    \caption{A binary decision tree}
    \label{fig:dst}
\end{figure}
To run the algorithm and compute $f$ given an input $\mathbf{x} \in \{0,1\}^N$, we start at the root node, which is associated with, say, variable $x_i$. We query $x_i$ and recurse on the left subtree if $x_i=1$ and on the right subtree otherwise. The recursion ends at one of the leaves, and the algorithm returns the Boolean value associated with the leaf. The depth of the tree determines the maximum number of queries before reaching a leaf. 

Under this view, an algorithm computing $f$ and its decision tree are equivalent. The algorithm computes the function, so does the decision tree. This motivates the following definition.
\begin{definition}[decision tree complexity]
The  \textit{decision tree complexity} $D(f)$ of a Boolean function $f: \{0,1\}^N \rightarrow \{0,1\}$ is the least depth among all decision trees that compute $f$.
\end{definition}
Given the definition, Conjecture~\ref{conj:1} can be re-stated as follows.
\begin{customconj}{1'}
    For any nontrivial monotone graph property $\mathcal{P}$ on $n$ vertices, $D(\mathcal{P}) = \binom{n}{2}$. 
\end{customconj}

\section{Topological Connections}
\label{sec:topo}
In this section, we introduce the notion of a simplicial complex. We will begin with its technical definition and build up some intuitions by considering its geometric realization. Some of the notations here draw on the lecture notes by L\'aszl\'o Lov\'asz \cite{DBLP:journals/corr/cs-CC-0205031}.
\begin{definition}[abstract simplicial complex]
    An \textit{abstract simplicial complex} is a nonempty collection $\Delta$ of sets such that if a set $Q$ is an element of $\Delta$, then all nonempty subsets of $Q$ must also be elements of $\Delta$.
\end{definition}
\begin{figure}[htbp]
    \centering
    \includegraphics[scale=0.8]{./img/sc.eps}
    \caption{Geometric representation of an abstract simplicial complex. We are not using the standard mapping defined below, since there is no higher-dimensional simplex than those in $\mathbb{R}^2$.}
    \label{fig:sc}
\end{figure}
We call the elements of the sets in $\Delta$ the \textit{vertices} of $\Delta$, denoted by $V(\Delta)$. Given any $\Delta$, one can construct its geometric realization $\widehat{\Delta} \in \mathbb{R}^N$, where $N = |V(\Delta)|$. First, define a mapping $\widehat{\cdot}: V(\Delta) \rightarrow \mathbb{R}^N$ such that no vertex is mapped into the affine hull of any other subset of vertices. Then map any set $Q$ of vertices to $\widehat{Q} =\text{conv}\{\widehat{v} : v\in Q\}$, and thereby $\Delta$ simply becomes $\widehat{\Delta} = \cup_{Q\in\Delta}\widehat{Q}$. Note that this maps every $k$-set in $\Delta$ to a $k-1$-simplex, where a point is a $0$-simplex, a line segment is a $1$-simplex, and so forth.

To draw a connection between monotone graph properties and simplicial complexes, we observe that the abstract simplicial complex associated with a property can be defined as follows. Let $V$ be a finite set and $E \subseteq V \times V$. Suppose $\mathcal{P}:\mathbf{G}(V) \rightarrow \{0,1\}$ be a monotone graph property. Then the \textit{abstract simplicial complex associated with} $\mathcal{P}$, denoted $\Delta(\mathcal{P})$, is the collection of all nonempty sets $E$ such that $\mathcal{P}((V, E))=0$. For example, consider the monotone property $\mathcal{P}$ where $\mathcal{P}(G) = 1$ if $G$ has at least $3$ edges.  Suppose $V = \{0,1,2,3\}$. Then $\Delta(\mathcal{P})$ looks like follows.
\begin{figure}[htbp]
    \centering
    \includegraphics[scale=.9]{./img/delta.eps}
    \caption{The abstract simplicial complex associated with $\mathcal{P}$.}
    \label{fig:delta}
\end{figure}

The next theorem relates an evasive graph property to a condition on its simplicial complex, called \textit{collapsibility}. We begin the some necessary definitions.
\begin{definition}
    Let $\Delta$ be an abstract simplicial complex and $Q \in \Delta$. Then $Q$ is a \textit{maximal} simplex if it is not contained in any other simplex in $\Delta$.
\end{definition}
\begin{definition}
    Let $\Delta$ be an abstract simplicial complex and $Q \in \Delta$. Then $Q$ is a \textit{free face} of $\Delta$ if it is not maximal and contained in only one maximal simplex in $\Delta$. If $Q$ is a free face and $Q'$ is the unique maximal simplex that contains it, then we say that $Q$ \textit{is a free face of} $Q'$
\end{definition}
\begin{definition}
    An \textit{elementary collapse} is the operation of choosing a free face from of an abstract simplicial complex and deleting it along with all the faces that contain it.
\end{definition}
\begin{definition}
    An elementary collapse is \textit{primitive} if the free face that is deleted  has dimension one less than the maximal simplex that contains it. 
\end{definition}
For example, let $\Delta = \{\{0\}, \{1\}, \{2\}, \{0,1\}, \{0,2\}, \{1,2\}, \{0,1,2\} \}$ be a $2$-simplex. Then $Q=\{0,1\}$ is a free face of $Q'=\{0,1,2\}$. By removing $Q$ and $Q'$ from $\Delta$, we obtain $\Delta' = \{\{0\},\{1\},\{2\},\{0,2\},\{1,2\}\}$. This is primitive elementary collapse.  
\begin{figure}[htbp]
    \centering
    \includegraphics[scale=0.9]{./img/collapse.eps}
    \caption{A primitive elementary collapse.}
    \label{fig:collapse}
\end{figure}
\begin{definition}
    Let $\Delta$ be an abstract simplicial complex. Then $\Delta$ is \textit{collapsible} if there exists a sequence of elementary collapses that reduces $\Delta$ to a single $0$-simplex. 
\end{definition}
Now we are ready to prove the theorem that relates the evasiveness of graph property to the collapsibility of the abstract simplicial complex associated with the property. The original proof by \cite{Kahn1984} requires a few more notions in topology, so here we present an elementary proof given by \cite{Miller:2013:EGP:2689479.2689480}.
\begin{theorem}
    \label{thm:collp}
    Let $\mathcal{P}$ be a nontrivial monotone graph property that is not evasive. Then $\Delta(\mathcal{P})$ is collapsible.
\end{theorem}
\begin{proof}
    Let $V$ be the vertex set of the graph and $|V|=n$.  Since $\mathcal{P}$ is not evasive, there exists some decision tree $T$of depth less than $\binom{n}{2}$. Without loss of generality, we assume that no edge appears more than once on any path in $T$. Also, for simplicity we assume that every path in $T$ has uniform length $\binom{n}{2}-1$. Otherwise, one can simply add redundant queries below every leaf that corresponds to a path of shorter length. 
\begin{figure}[]
    \centering
    \includegraphics[scale=0.9]{./img/uni.eps}
    \caption{A decision tree where every path is of same length.}
    \label{fig:uni}
\end{figure}

Now we can define a natural ordering on the leaves by asserting that for any internal node, all leaves that can be reached from the ``$1$'' branch are smaller than those that can be reached from the ``$0$'' branch. Since every pair of leaves shares a common ancestor, this gives  a total order on all leaves.

Observe that for any leaf of the tree, there are exactly two graphs that will lead the decision tree to this leaf. The simplicial complex $\Delta(\mathcal{P})$ is composed of the graphs that correspond to those ``$0$''-leaves. The ordering of the leaves provides a sequence of elementary collapses that reduces $\Delta(\mathcal{P})$ to a point. First we find the smallest ``$0$''-leaf of $T$. This leaf corresponds to a pair of simplices $Q_1, Q_2\in \Delta(\mathcal{P})$ with $Q_1\subseteq Q_2$, since the path that ends at this leaf has length $\binom{n}{2}-1$.  It follows from the ordering of the leaves that neither $Q_1$ nor $Q_2$ are not contained in any other simplices in $\Delta(\mathcal{P})$. Thus, $Q_1$ is a free face. Hence, we perform the elementary collapse:
\[
    \Delta_1(\mathcal{P}) \leftarrow \Delta(\mathcal{P}) \setminus \{Q_1,Q_2\}.
\]
Next recurse on $\Delta_1(\mathcal{P})$ by finding the second smallest leaf and performing another elementary collapse until all simplices on ``$0$''-leaves are removed and the simplicial complex is reduced to a point.
\end{proof}
We have so far established a link between evasiveness and collapsibility of simplicial complex. That is, a monotone graph property is evasive if the associated simplicial complex is not collapsible. We need more topology to show that in some cases the complexes are indeed not collapsible, and a key step is to establish the topological fixed-point theorems.
\section{Fixed-Point Theorems} 
\label{sec:fixed}
In this section, we discuss other necessary topological backgrounds for understanding the results on graph evasiveness, particularly the fixed-point theorems. The proofs will be largely omitted, and the reader is invited to read \cite{Miller:2013:EGP:2689479.2689480} or some standard texts in algebraic topology for the full proofs.

\begin{figure}[ht]
    \centering
    \includegraphics[scale=1]{./img/fixp.pdf}
    \caption{Fixed points of $3$-simplex (in courtesy of \cite{DBLP:journals/corr/cs-CC-0205031})}
    \label{fig:fixp}
\end{figure}
Standard fixed-point theorems in topology tells us that a continuous map of a collapsible polyhedron onto itself has a fixed point, \textit{i.e.}, a point that is mapped to itself. On the other hand, the fact that a nontrivial monotone property $\mathcal{P}$ is invariant under permutation implies that the geometric realization of the permutation maps  $\widehat{\Delta(\mathcal{P})}$ to itself, and thus has a fixed point if $\mathcal{P}$ is not evasive. The hope is that we can characterize the fixed points and deduce that the cannot exist unless $\mathcal{P}$ is trivial, leading to a contradiction.
\begin{definition}
Let $\Delta$ be a simplicial complex on the ground set $V$. A bijection $\phi : V \rightarrow V$ is an \textit{automorphism of} $\Delta$  if for each $Q \in \Delta$, we have $\phi(Q) \in \Delta$, where $\phi(Q) = \{\phi(v) : v\in Q\}$. 
\end{definition}
There are numerous classical fixed-point theorems. The following is a special case of a result due to Lefshetz \cite{10.2307/1989171} that is a generalization of the well-known Brouwer’s fixed-point theorem.
\begin{theorem}[Lefshetz]
\label{thm:lef}
Every automorphism of (the geometric realization of) a non-empty collapsible abstract simplicial complex has a fixed point.
\end{theorem}
We denote the set of fixed points of $\widehat{\Delta}$ under an automorphism $\phi$ by fix$(\phi)$. It turns out that there is a nice characterization of the set. 
\begin{definition}
    Let $\Delta$ be an abstract simplicial complex on vertex set $V$, and $f:V\rightarrow V$ be a automorphism of $\Delta$. Let $A_1,\cdots,A_m \subseteq V$ denote the orbits of $f$. Then $\Delta^{f}$ denote the set of all subsets $T\subseteq \{A_1,\cdots, A_m\}$ such that 
    \[
        \bigcup_{A\subseteq T} A \in \Delta.
    \]
\end{definition}
Observe that $\Delta^f$ is an abstract simplicial complex whose vertices are all those orbits that are contained in $\Delta$. We define the geometric realization of $\Delta^f$ by taking every one of its vertices to be the center of mass of the face $\widehat{A_i}$ for $A_i \in \Delta$, denoted $\widehat{\Delta^f}$. The fixed-point set also forms a simplicial complex, and geometrically it is generated by taking the convex hull of the centers of mass of the orbits contained in $\Delta$.
\begin{theorem}
\label{thm:fixphi}
    Let $\phi$ be an automorphism of a simplicial complex $\Delta$. Then fix($\phi$)$=\widehat{\Delta^{\phi}}$.
\end{theorem}
Our first result in the next section will be on the bipartite graph properties, for which we need a form of Hopf index formula. Let us first recall the Euler characteristic. For our purpose, we give a definition that differs slightly from the standard one.
\begin{definition}[Euler characteristic]
    \label{def:euler}
    The \textit{Euler characteristic} of a simplicial complex $\widehat{\Delta}$ is defined by
    \[
        \chi\left(\widehat{\Delta}\right) = \sum_{Q\in \Delta, Q\neq \emptyset} (-1)^{|Q|}.
    \]
\end{definition}
Note that the Euler characteristic is invariant of topological deformation, \textit{i.e.}, homotopy invariant, and that a collapsible simplex has Euler characteristic $-1$.
\begin{theorem}[Hopf index theorem]
    \label{thm:hopf}
    Let $\phi$ be an automorphism of a nonempty collapsible simplicial complex, then $\chi(\text{fix}(\phi)) = -1$.
\end{theorem}

\section{Evasiveness Results}
\label{sec:evas}
Let us start with a result on the evasiveness of bipartite graph properties, due to Andrew Yao \cite{Yao:1988:MBG:44082.44088}. Let $U$ and $V$ be two disjoint ground sets and $\mathbf{G}(U,V)$ be the set of all bipartite graphs defined on the bipartition $U,V$. Then naturally, define a \textit{bipartite graph property} by a function $f:\mathbf{G}(U,V)\rightarrow \{0,1\}$ that is invariant under isomorphisms. Additionally, the property is monotone if adding edges between $U$ and $V$ does not destroy the property. Given the definitions, we say that a bipartite graph property $f$ is evasive if its decision tree complexity $D(f)$ equals $|U|\cdot |V|$.
\begin{theorem}[\cite{Yao:1988:MBG:44082.44088}]
Every nontrivial monotone bipartite graph property is evasive.
\end{theorem}
\begin{proof}
    Let $f$ be a nontrivial monotone bipartite graph property on $\mathbf{G}(U,V)$. Suppose for a contradiction that it is not evasive. Let $\Delta(f)$ be the simplicial complex associated with $f$, and recall that its ground elements correspond to edges of the bipartite graphs in $\mathbf{G}(U,V)$. Let $\phi$ be a permutation of these edges induced by a cyclic permutation of vertices in $U$, while fixing $V$ unchanged.  It is easy to observe that $\phi$ is an automorphism of $\Delta(f)$. The orbits of $\phi$ are the sets of edges that share a common vertex in $A$, and we will index them by elements of $A$. 
    
    By Theorem~\ref{thm:collp}, the complex $\Delta(f)$ is collapsible, and therefore has a fixed point by the Lefshetz fixed-point theorem. It follows from Theorem~\ref{thm:fixphi} that there exists an orbit of $\phi$ that is an element of $\Delta(f)$. Suppose that $\{u'\} \times V$ is such an orbit. But since $f$ is invariant under isomorphism, we deduce that every orbit $\{u\}\times V$ is in $\Delta(f)$. Therefore, the set $\Delta^{\phi} \subseteq \Delta$, which determines the fixed point set, is of the form $U' \times V$, where $U' \subseteq U$. By invariance under isomorphism  and  monotonicity, $\Delta^f$ can be characterized as
    \[
        \Delta^f =\{U'\times V : |U'| \le c\}
    \]
    for some constant $c\le |V|$.

    Let us now calculate the Euler characteristic of $\widehat{\Delta^f}$. 
    \begin{align*}
        \chi\left(\widehat{\Delta^f}\right) &= \sum_{i=1}^c (-1)^i \binom{|U|}{i} && \text{(by Definition~\ref{def:euler})} \\
                                            &=\sum_{i=1}^c (-1)^i \left(\binom{|U|-1}{i-1} + \binom{|U|-1}{i}\right) &&\text{(by Pascal's identity)}\\
                       &= -1 + (-1)^{c}\binom{|U|-1}{c}. 
    \end{align*}
    Since $\Delta$ is non-evasive and thus collapsible, by Theorem~\ref{thm:hopf}, $\chi(\text{fix}(\phi)) = \chi\left(\widehat{\Delta^f}\right)  = -1$. This implies that $\binom{|U|-1}{c}=0$, which is the case only if $c=|U|$. But then $\Delta^f =\{U\times V\} \subseteq \Delta$, and it follows that $\Delta$ is trivial, a contradiction.
\end{proof}
One might expect that the similar proof technique works out for general graph properties. The orbits of a permutation on a general graph, however, is much more complicated to characterize, and therefore it is hard to deduce anything using the fixed-point theorems. Kahn, Saks, and Sturtevant showed a surprising result that when the number of vertices is a prime power, the problem is nevertheless tractable~\cite{Kahn1984}. Their proof relies on a more general fixed-point theorem as follows.
\begin{theorem}[\cite{Oliver1975}]
\label{thm:oliver}
    Let $\Delta$ be a collapsible abstract simplicial complex and $\Gamma$ be a group of automorphisms of $\Delta$. Suppose $\Gamma'$ is a normal subgroup of $\Gamma$ such that $|\Gamma'|=p^k$ for some prime $p$ and integer $k\ge 1$ and the quotient group $\Gamma / \Gamma'$ is cyclic. Then there eixsts $x\in \widehat{\Delta}$ fixed by every $\phi \in \Gamma$.
\end{theorem}
\begin{theorem}[\cite{Kahn1984}]
    Let $V$ be a finite ground set of order $p^k$ for some prime $p$ and integer $k\geq 1$, Then every nontrivial monotone graph property $\mathcal{P}$ on $V$ is evasive.
\end{theorem}
\begin{proof}
    Assume that without loss of generality the vertex set $V$ is the set of elements of the Galois field $\mathbb{F}_{p^k}$. Consider the group $\Gamma$ of permuatations of $V$ given by the linear mappings
    \[
        x \mapsto ax+b: \mathbb{F}_{p^k} \rightarrow \mathbb{F}_{p^k}.
    \]
Let $\Gamma' \subseteq \Gamma$ be the set of  mappings $x\mapsto x+b$. It is easy to observe that $\Gamma'$ is a group.  To apply the preceding theorem, we make the following observations.
\begin{itemize}
    \item The subgroup $\Gamma'$ is normal. For any $x,a,b\in \mathbb{F}_{p^k}$ such that $a\neq 0$, we have 
        \[
            a^{-1}(ax+b) = x+a^{-1}b.
        \]
    \item The factor group $\Gamma / \Gamma'$ is cyclic, since it is isomorphic to the group of mappings $x \mapsto ax : a\neq 0$, which in turn is isomorphic to the multiplicative group of $\mathbb{F}_{p^k}$.
\end{itemize}
Let $\mathcal{P}$ be a nontrivial monotone graph property, and suppose it is not evasive. As $\Delta(\mathcal{P})$ is collapsible, by Theorem~\ref{thm:oliver}, there exists $G \in \widehat{\Delta(\mathcal{P})}$ that is fixed by every $\phi \in \Gamma$; that is, $G$ is a $\Gamma$-invariant graph which does not satisfies $\mathcal{P}$. Now obverse that $\Gamma$ is transitive on edges, since for any pair of distince elements $(u,u'), (v,v') \in V\times V$, the system of equations 
\begin{align*}
    ax+b &=y\\
    ax'+b&=y'
\end{align*}
has a solution with $a\neq 0$. This implies that the only nonempty $\Gamma$-invariant graph is the complete graph, and hence, the property $\mathcal{P}$ is trivial, a contradiction.
\end{proof}
We have shown our two classic evasiveness results obtained using topological methods. This line of works has been fruitful over the last two decades. Next, we will briefly discuss some recent progress.

\section{Recent Progress}
The earliest quadratic lower bound on the deterministic decision tree complexity of testing nontrivial monotone graph property was obtained by~\cite{rivest1975generalization} proving that $n^2/16$ queries are required. The classic paper~\cite{Kahn1984} also shows a $n^2/4-o(n^2)$ lower bound for the decision tree complexity. This has been further improved to $(8/25)n^2-o(n^2)$ by~\cite{Korneffel2010} and to $n^2/3 -o(n^2)$ by~\cite{scheidweiler2013lower}, both using topological approach.  The last remains that best bound thus far.

A notatble work by Chakrabarti, Khot, and Shi~\cite{chakrabarti2001evasiveness} extends the technique in~\cite{Kahn1984}. They showed that every minor-closed property, including planarity, is evasive for sufficiently large $n$ and that the property of containing a fixed subgraph is evasive  for an arithmetic progression of values of $n$.

Karp also conjectured that $\Omega(n^2)$ queries are required for testing nontrivial monotone graph properties even if randomization is allowed. The first superlinear lower bound was given by~\cite{YAO1991267} showing that $\Omega(n\log^{1/12}n)$ queries are required. The bound was later improved by \cite{King:1988:LBC:62212.62258} to $\Omega(n^{5/4})$, by \cite{Hajnal1991} to $\Omega(n^{4/3})$. The current best bound of $\Omega(n^{4/3}\log^{1/3}n)$ was given by \cite{chakrabarti2007improved}. We remark, however, that topological constructions have not been applied to studying the randomized query complexity.
\label{sec:more}

\section{Acknowledgement}
I would like to thank John Reif for teaching \textsc{CompSci} 534: Computational Complexity at Duke University in Spring 17, from which I learned a lot about complexity theory.

\bibliographystyle{alpha}
\bibliography{references}
\end{document}
